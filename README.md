# Tiny-Uart

#### 介绍
pyqt5+pyserial制作一个简单的串口工具

#### 软件架构

软件架构说明
UI界面使用qt designer进行设计
功能实现使用python

#### 文件说明

ico:
- ico.qrc->qt designer资源文件

src:
- ico_rc.py->资源文件转换成py格式
- packaging.py->执行cmd命令，打包程序
- tiny_uart_main.py->主程序
- tiny_uart.ui->qt designer设计的UI文件
- Ui_tiny_uart.py->UI文件转换成py格式

#### 环境搭建

见doc中***环境搭建.md***

#### 打包

1.  安装pyinstaller
2.  运行src/packaging.py
3.  dist文件夹中将会生成exe文件

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

>pyqt5接口文档：https://www.riverbankcomputing.com/static/Docs/PyQt5/#